/**
 * @author Sudikshya Bisoyi
 *
 */

package time;

import org.junit.Test;

public class TimeTest {

	@Test
	public void testGetTotalSecondsRegular() {
		int totalSeconds = Time.getTotalSeconds("01:01:01");
		
		assertTrue( "The time provided matchs the result", totalSeconds==3661);
		
	}

	 private void assertTrue(String string, boolean b) {
		// TODO Auto-generated method stub
		
	}

	@Test (expected=NumberFormatException.class)
	 public void testGetTotalSecondsException() {
		 int totalSeconds= Time.getTotalSeconds("01:01:0A");
		 fail (" The time provided is not valid");
	 }
	 
	 private void fail(String string) {
		// TODO Auto-generated method stub
		
	}

	@Test 
	 public void testGetTotalSecondsBoundaryIn() {
		 int totalSeconds= Time.getTotalSeconds("00:00:59");
		 assertTrue (" The time provided almost matchs the result", totalSeconds == 59);
	 }
	 
	 @Test (expected=NumberFormatException.class)
	 public void testGetTotalSecondsBoundaryOut() {
		 int totalSeconds= Time.getTotalSeconds("01:01:60");
		 fail ("The time provided is not valid");
	 }
}
